1
00:00:06,694 --> 00:00:09,979
Right now, you're probably sitting down
to watch this video

2
00:00:09,979 --> 00:00:14,599
and staying seated for a few minutes
to view it is probably okay.

3
00:00:14,599 --> 00:00:19,172
But the longer you stay put,
the more agitated your body becomes.

4
00:00:19,172 --> 00:00:23,048
It sits there counting down the moments
until you stand up again

5
00:00:23,048 --> 00:00:25,011
and take it for a walk.

6
00:00:25,011 --> 00:00:26,711
That may sound ridiculous.

7
00:00:26,711 --> 00:00:29,156
Our bodies love to sit, right?

8
00:00:29,156 --> 00:00:30,477
Not really.

9
00:00:30,477 --> 00:00:34,25
Sure, sitting for brief periods 
can help us recover from stress

10
00:00:34,25 --> 00:00:36,647
or recuperate from exercise.

11
00:00:36,647 --> 00:00:42,206
But nowadays, our lifestyles make us sit
much more than we move around,

12
00:00:42,206 --> 00:00:46,286
and our bodies simply aren't built
for such a sedentary existence.

13
00:00:46,286 --> 00:00:49,089
In fact, just the opposite is true.

14
00:00:49,089 --> 00:00:51,653
The human body is built to move,

15
00:00:51,653 --> 00:00:54,841
and you can see evidence of that
in the way it's structured.

16
00:00:54,841 --> 00:01:00,205
Inside us are over 360 joints,
and about 700 skeletal muscles

17
00:01:00,205 --> 00:01:03,352
that enable easy, fluid motion.

18
00:01:03,352 --> 00:01:07,721
The body's unique physical structure
gives us the ability to stand up straight

19
00:01:07,721 --> 00:01:09,908
against the pull of gravity.

20
00:01:09,908 --> 00:01:14,232
Our blood depends on us moving around
to be able to circulate properly.

21
00:01:14,232 --> 00:01:16,713
Our nerve cells benefit from movement,

22
00:01:16,713 --> 00:01:21,44
and our skin is elastic, 
meaning it molds to our motions.

23
00:01:21,44 --> 00:01:25,426
So if every inch of the body
is ready and waiting for you to move,

24
00:01:25,426 --> 00:01:28,798
what happens when you just don't?

25
00:01:28,798 --> 00:01:32,105
Let's start with the backbone
of the problem, literally.

26
00:01:32,105 --> 00:01:34,128
Your spine is a long structure

27
00:01:34,128 --> 00:01:38,044
made of bones and the cartilage discs
that sit between them.

28
00:01:38,044 --> 00:01:41,451
Joints, muscles and ligaments
that are attached to the bones

29
00:01:41,451 --> 00:01:43,376
hold it all together.

30
00:01:43,376 --> 00:01:47,651
A common way of sitting is with a
curved back and slumped shoulders,

31
00:01:47,651 --> 00:01:51,379
a position that puts uneven
pressure on your spine.

32
00:01:51,379 --> 00:01:55,331
Over time, this causes wear and tear
in your spinal discs,

33
00:01:55,331 --> 00:01:57,873
overworks certain ligaments and joints,

34
00:01:57,873 --> 00:02:00,916
and puts strain on muscles that stretch

35
00:02:00,916 --> 00:02:04,355
to accommodate 
your back's curved position.

36
00:02:04,355 --> 00:02:08,312
This hunched shape also shrinks
your chest cavity while you sit,

37
00:02:08,312 --> 00:02:13,322
meaning your lungs have less space
to expand into when you breath.

38
00:02:13,322 --> 00:02:17,234
That's a problem because it temporarily
limits the amount of oxygen

39
00:02:17,234 --> 00:02:20,931
that fills your lungs
and filters into your blood.

40
00:02:20,931 --> 00:02:25,694
Around the skeleton are the muscles,
nerves, arteries and veins

41
00:02:25,694 --> 00:02:28,678
that form the body's soft tissue layers.

42
00:02:28,678 --> 00:02:33,527
The very act of sitting squashes,
pressurizes and compresses,

43
00:02:33,527 --> 00:02:37,033
and these more delicate tissues
really feel the brunt.

44
00:02:37,033 --> 00:02:41,282
Have you ever experienced numbness
and swelling in your limbs when you sit?

45
00:02:41,282 --> 00:02:43,46
In areas that are the most compressed,

46
00:02:43,46 --> 00:02:46,799
your nerves, arteries and veins
can become blocked,

47
00:02:46,799 --> 00:02:50,24
which limits nerve signaling,
causing the numbness,

48
00:02:50,24 --> 00:02:55,421
and reduces blood flow in your limbs,
causing them to swell.

49
00:02:55,421 --> 00:03:00,879
Sitting for long periods also temporarily
deactivates lipoprotein lipase,

50
00:03:00,879 --> 00:03:04,759
a special enzyme in the walls
of blood capillaries

51
00:03:04,759 --> 00:03:07,362
that breaks down fats in the blood,

52
00:03:07,362 --> 00:03:13,016
so when you sit, you're not burning fat
nearly as well as when you move around.

53
00:03:13,016 --> 00:03:16,268
What effect does all of this stasis
have on the brain?

54
00:03:16,268 --> 00:03:19,512
Most of the time, 
you probably sit down to use your brain,

55
00:03:19,512 --> 00:03:24,568
but ironically, lengthy periods of sitting
actually run counter to this goal.

56
00:03:24,568 --> 00:03:26,968
Being stationary reduces blood flow

57
00:03:26,968 --> 00:03:30,856
and the amount of oxygen entering
your blood stream through your lungs.

58
00:03:30,856 --> 00:03:34,324
Your brain requires both 
of those things to remain alert,

59
00:03:34,324 --> 00:03:37,629
so your concentration levels
will most likely dip

60
00:03:37,629 --> 00:03:40,783
as your brain activity slows.

61
00:03:40,783 --> 00:03:45,989
Unfortunately, the ill effects of being
seated don't only exist in the short term.

62
00:03:45,989 --> 00:03:48,625
Recent studies have found 
that sitting for long periods

63
00:03:48,625 --> 00:03:52,322
is linked with some types of cancers
and heart disease

64
00:03:52,322 --> 00:03:57,107
and can contribute to diabetes,
kidney and liver problems.

65
00:03:57,107 --> 00:04:00,768
In fact, researchers 
have worked out that, worldwide,

66
00:04:00,768 --> 00:04:05,773
inactivity causes about 
9% of premature deaths a year.

67
00:04:05,773 --> 00:04:08,604
That's over 5 million people.

68
00:04:08,604 --> 00:04:10,704
So what seems like such a harmless habit

69
00:04:10,704 --> 00:04:14,102
actually has the power 
to change our health.

70
00:04:14,102 --> 00:04:19,674
But luckily, the solutions to this
mounting threat are simple and intuitive.

71
00:04:19,674 --> 00:04:21,436
When you have no choice but to sit,

72
00:04:21,436 --> 00:04:25,149
try switching the slouch 
for a straighter spine,

73
00:04:25,149 --> 00:04:27,54
and when you don't have 
to be bound to your seat,

74
00:04:27,54 --> 00:04:30,396
aim to move around much more,

75
00:04:30,396 --> 00:04:35,571
perhaps by setting a reminder
to yourself to get up every half hour.

76
00:04:35,571 --> 00:04:41,073
But mostly, just appreciate that bodies
are built for motion, not for stillness.

77
00:04:41,073 --> 00:04:46,112
In fact, since the video's almost over,
why not stand up and stretch right now?

78
00:04:46,112 --> 00:04:47,821
Treat your body to a walk.

79
00:04:47,821 --> 00:04:49,936
It'll thank you later.

