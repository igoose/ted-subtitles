1
00:00:07,139 --> 00:00:10,579
Why is it so difficult to cure cancer?

2
00:00:10,579 --> 00:00:12,3
We've harnessed electricity,

3
00:00:12,3 --> 00:00:14,129
sequenced the human genome,

4
00:00:14,129 --> 00:00:16,741
and eradicated small pox.

5
00:00:16,741 --> 00:00:18,94
But after billions of dollars in research,

6
00:00:18,94 --> 00:00:23,121
we haven't found a solution for a disease
that affects more than 14 million people

7
00:00:23,121 --> 00:00:27,061
and their families at any given time.

8
00:00:27,061 --> 00:00:31,72
Cancer arises as normal cells
accumulate mutations.

9
00:00:31,72 --> 00:00:35,496
Most of the time, 
cells can detect mutations or DNA damage

10
00:00:35,496 --> 00:00:38,862
and either fix them or self destruct.

11
00:00:38,862 --> 00:00:43,117
However, some mutations allow cancerous
cells to grow unchecked

12
00:00:43,117 --> 00:00:45,173
and invade nearby tissues,

13
00:00:45,173 --> 00:00:48,816
or even metastasize to distant organs.

14
00:00:48,816 --> 00:00:54,082
Cancers become almost incurable
once they metastasize.

15
00:00:54,082 --> 00:00:57,002
And cancer is incredibly complex.

16
00:00:57,002 --> 00:00:58,873
It's not just one disease.

17
00:00:58,873 --> 00:01:00,872
There are more than 100 different types

18
00:01:00,872 --> 00:01:05,363
and we don't have a magic bullet
that can cure all of them.

19
00:01:05,363 --> 00:01:06,731
For most cancers,

20
00:01:06,731 --> 00:01:10,933
treatments usually include 
a combination of surgery to remove tumors

21
00:01:10,933 --> 00:01:16,247
and radiation and chemotherapy
to kill any cancerous cells left behind.

22
00:01:16,247 --> 00:01:17,507
Hormone therapies,

23
00:01:17,507 --> 00:01:18,948
immunotherapy,

24
00:01:18,948 --> 00:01:22,37
and targeted treatments tailored
for a specific type of cancer

25
00:01:22,37 --> 00:01:25,219
are sometimes used, too.

26
00:01:25,219 --> 00:01:27,609
In many cases, 
these treatments are effective

27
00:01:27,609 --> 00:01:30,27
and the patient becomes cancer-free.

28
00:01:30,27 --> 00:01:35,809
But they're very far from 100% effective
100% of the time.

29
00:01:35,809 --> 00:01:40,54
So what would we have to do to find cures
for all the different forms of cancer?

30
00:01:40,54 --> 00:01:43,06
We're beginning to understand a few
of the problems

31
00:01:43,06 --> 00:01:45,83
scientists would have to solve.

32
00:01:45,83 --> 00:01:50,379
First of all, we need new, better ways
of studying cancer.

33
00:01:50,379 --> 00:01:54,19
Most cancer treatments are developed
using cell lines grown in labs

34
00:01:54,19 --> 00:01:56,95
from cultures of human tumors.

35
00:01:56,95 --> 00:01:59,422
These cultured cells have given us
critical insights

36
00:01:59,422 --> 00:02:01,74
about cancer genetics and biology,

37
00:02:01,74 --> 00:02:06,671
but they lack much of the complexity
of a tumor in an actual living organism.

38
00:02:06,671 --> 00:02:10,871
It's frequently the case that new drugs,
which work on these lab-grown cells,

39
00:02:10,871 --> 00:02:14,502
will fail in clinical trials 
with real patients.

40
00:02:14,502 --> 00:02:17,431
One of the complexities 
of aggressive tumors

41
00:02:17,431 --> 00:02:23,296
is that they can have multiple populations
of slightly different cancerous cells.

42
00:02:23,296 --> 00:02:27,067
Over time, distinct genetic mutations
accumulate in cells

43
00:02:27,067 --> 00:02:33,042
in different parts of the tumor,
giving rise to unique subclones.

44
00:02:33,042 --> 00:02:37,061
For example, aggressive brain tumors
called glioblastomas

45
00:02:37,061 --> 00:02:41,713
can have as many as six different 
subclones in a single patient.

46
00:02:41,713 --> 00:02:44,437
This is called clonal heterogeneity,

47
00:02:44,437 --> 00:02:48,232
and it makes treatment difficult because
a drug that works on one subclone

48
00:02:48,232 --> 00:02:51,123
may have no effect on another.

49
00:02:51,123 --> 00:02:52,923
Here's another challenge.

50
00:02:52,923 --> 00:02:55,784
A tumor is a dynamic 
interconnected ecosystem

51
00:02:55,784 --> 00:02:58,543
where cancer cells constantly 
communicate with each other

52
00:02:58,543 --> 00:03:01,265
and with healthy cells nearby.

53
00:03:01,265 --> 00:03:05,052
They can induce normal cells to form
blood vessels that feed the tumor

54
00:03:05,052 --> 00:03:07,903
and remove waste products.

55
00:03:07,903 --> 00:03:10,249
They can also interact 
with the immune system

56
00:03:10,249 --> 00:03:12,196
to actually suppress its function,

57
00:03:12,196 --> 00:03:16,265
keeping it from recognizing 
or destroying the cancer.

58
00:03:16,265 --> 00:03:19,415
If we could learn how to shut down
these lines of communication,

59
00:03:19,415 --> 00:03:23,733
we'd have a better shot at vanquishing 
a tumor permanently.

60
00:03:23,733 --> 00:03:26,024
Additionally, mounting evidence suggests

61
00:03:26,024 --> 00:03:30,505
we'll need to figure out how to eradicate
cancer stem cells.

62
00:03:30,505 --> 00:03:33,026
These are rare but seem 
to have special properties

63
00:03:33,026 --> 00:03:36,748
that make them resistant 
to chemotherapy and radiation.

64
00:03:36,748 --> 00:03:41,486
In theory, even if the rest of the tumor
shrinks beyond detection during treatment,

65
00:03:41,486 --> 00:03:47,654
a single residual cancer stem cell
could seed the growth of a new tumor.

66
00:03:47,654 --> 00:03:49,957
Figuring out how to target
these stubborn cells

67
00:03:49,957 --> 00:03:54,146
might help prevent cancers 
from coming back.

68
00:03:54,146 --> 00:03:57,536
Even if we solved those problems,
we might face new ones.

69
00:03:57,536 --> 00:04:00,667
Cancer cells are masters of adaptation,

70
00:04:00,667 --> 00:04:05,938
adjusting their molecular and cellular
characteristics to survive under stress.

71
00:04:05,938 --> 00:04:08,868
When they're bombarded by radiation
or chemotherapy,

72
00:04:08,868 --> 00:04:12,779
some cancer cells can effectively
switch on protective shields

73
00:04:12,779 --> 00:04:17,697
against whatever's attacking them
by changing their gene expression.

74
00:04:17,697 --> 00:04:21,846
Malignant cancers are complex systems
that constantly evolve and adapt.

75
00:04:21,846 --> 00:04:25,301
To defeat them, we need to find
experimental systems

76
00:04:25,301 --> 00:04:27,333
that match their complexity,

77
00:04:27,333 --> 00:04:32,767
and monitoring and treatment options
that can adjust as the cancer changes.

78
00:04:32,767 --> 00:04:35,87
But the good news is 
we're making progress.

79
00:04:35,87 --> 00:04:37,28
Even with all we don't know,

80
00:04:37,28 --> 00:04:39,85
the average mortality rate 
for most kinds of cancer

81
00:04:39,85 --> 00:04:44,869
has dropped significantly since the 1970s
and is still falling.

82
00:04:44,869 --> 00:04:46,36
We're learning more every day,

83
00:04:46,36 --> 00:04:50,729
and each new piece of information gives
us one more tool to add to our arsenal.

