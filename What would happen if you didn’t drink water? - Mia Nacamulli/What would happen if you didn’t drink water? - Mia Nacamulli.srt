1
00:00:06,505 --> 00:00:08,807
Water is virtually everywhere,

2
00:00:08,807 --> 00:00:11,259
from soil moisture and ice caps,

3
00:00:11,259 --> 00:00:14,086
to the cells inside our own bodies.

4
00:00:14,086 --> 00:00:16,181
Depending on factors like location,

5
00:00:16,181 --> 00:00:17,399
fat index,

6
00:00:17,399 --> 00:00:18,114
age,

7
00:00:18,114 --> 00:00:19,202
and sex,

8
00:00:19,202 --> 00:00:23,808
the average human is between 55-60% water.

9
00:00:23,808 --> 00:00:27,024
At birth, human babies are even wetter.

10
00:00:27,024 --> 00:00:32,307
Being 75% water, 
they are swimmingly similar to fish.

11
00:00:32,307 --> 00:00:37,331
But their water composition drops to 65%
by their first birthday.

12
00:00:37,331 --> 00:00:40,196
So what role does water 
play in our bodies,

13
00:00:40,196 --> 00:00:44,288
and how much do we actually need to drink
to stay healthy?

14
00:00:44,288 --> 00:00:48,529
The H20 in our bodies works to cushion
and lubricate joints,

15
00:00:48,529 --> 00:00:49,948
regulate temperature,

16
00:00:49,948 --> 00:00:53,932
and to nourish the brain and spinal cord.

17
00:00:53,932 --> 00:00:56,062
Water isn't only in our blood.

18
00:00:56,062 --> 00:01:00,51
An adult's brain and heart are almost
three quarters water.

19
00:01:00,51 --> 00:01:04,367
That's roughly equivalent to the amount
of moisture in a banana.

20
00:01:04,367 --> 00:01:08,487
Lungs are more similar to an apple at 83%.

21
00:01:08,487 --> 00:01:14,31
And even seemingly dry human bones
are 31% water.

22
00:01:14,31 --> 00:01:16,051
If we are essentially made of water,

23
00:01:16,051 --> 00:01:17,634
and surrounded by water,

24
00:01:17,634 --> 00:01:20,492
why do we still need to drink so much?

25
00:01:20,492 --> 00:01:24,293
Well, each day we lose two to three liters
through our sweat,

26
00:01:24,293 --> 00:01:25,227
urine,

27
00:01:25,227 --> 00:01:26,707
and bowel movements,

28
00:01:26,707 --> 00:01:28,371
and even just from breathing.

29
00:01:28,371 --> 00:01:30,859
While these functions 
are essential to our survival,

30
00:01:30,859 --> 00:01:33,306
we need to compensate for the fluid loss.

31
00:01:33,306 --> 00:01:37,848
Maintaining a balanced water level
is essential to avoid dehydration

32
00:01:37,848 --> 00:01:39,71
or over-hydration,

33
00:01:39,71 --> 00:01:43,279
both of which can have devastating
effects on overall health.

34
00:01:43,279 --> 00:01:45,404
At first detection of low water levels,

35
00:01:45,404 --> 00:01:48,367
sensory receptors 
in the brain's hypothalamus

36
00:01:48,367 --> 00:01:51,399
signal the release 
of antidiuretic hormone.

37
00:01:51,399 --> 00:01:54,489
When it reached the kidneys, 
it creates aquaporins,

38
00:01:54,489 --> 00:02:00,445
special channels that enable blood
to absorb and retain more water,

39
00:02:00,445 --> 00:02:03,05
leading to concentrated, dark urine.

40
00:02:03,05 --> 00:02:06,555
Increased dehydration can cause
notable drops in energy,

41
00:02:06,555 --> 00:02:07,586
mood,

42
00:02:07,586 --> 00:02:08,774
skin moisture,

43
00:02:08,774 --> 00:02:10,198
and blood pressure,

44
00:02:10,198 --> 00:02:13,252
as well as signs of cognitive impairment.

45
00:02:13,252 --> 00:02:17,21
A dehydrated brain works harder 
to accomplish the same amount

46
00:02:17,21 --> 00:02:18,696
as a normal brain,

47
00:02:18,696 --> 00:02:22,618
and it even temporarily shrinks
because of its lack of water.

48
00:02:22,618 --> 00:02:25,614
Over-hydration, or hyponatremia,

49
00:02:25,614 --> 00:02:30,669
is usually caused by overconsumption
of water in a short amount of time.

50
00:02:30,669 --> 00:02:33,242
Athletes are often the victims 
of over-hydration

51
00:02:33,242 --> 00:02:36,585
because of complications 
in regulating water levels

52
00:02:36,585 --> 00:02:39,263
in extreme physical conditions.

53
00:02:39,263 --> 00:02:43,769
Whereas the dehydrated brain amps
up the production of antidiuretic hormone,

54
00:02:43,769 --> 00:02:49,174
the over-hydrated brain slows,
or even stops, releasing it into the blood.

55
00:02:49,174 --> 00:02:51,98
Sodium electrolytes in the body
become diluted,

56
00:02:51,98 --> 00:02:54,133
causing cells to swell.

57
00:02:54,133 --> 00:02:55,584
In severe cases,

58
00:02:55,584 --> 00:02:59,768
the kidneys can't keep up with 
the resulting volumes of dilute urine.

59
00:02:59,768 --> 00:03:01,923
Water intoxication then occurs,

60
00:03:01,923 --> 00:03:03,559
possibly causing headache,

61
00:03:03,559 --> 00:03:04,688
vomiting,

62
00:03:04,688 --> 00:03:08,162
and, in rare instances, seizures
or death.

63
00:03:08,162 --> 00:03:10,295
But that's a pretty extreme situation.

64
00:03:10,295 --> 00:03:12,062
On a normal, day-to-day basis,

65
00:03:12,062 --> 00:03:15,632
maintaining a well-hydrated system
is easy to manage

66
00:03:15,632 --> 00:03:19,528
for those of us fortunate enough 
to have access to clean drinking water.

67
00:03:19,528 --> 00:03:23,808
For a long time, conventional wisdom said
that we should drink eight glasses a day.

68
00:03:23,808 --> 00:03:26,453
That estimate has since been fine-tuned.

69
00:03:26,453 --> 00:03:30,245
Now, the consensus is that the amount
of water we need to imbibe

70
00:03:30,245 --> 00:03:33,116
depends largely on our weight
and environment.

71
00:03:33,116 --> 00:03:39,616
The recommended daily intake varies from
between 2.5-3.7 liters of water for men,

72
00:03:39,616 --> 00:03:42,802
and about 2-2.7 liters for women,

73
00:03:42,802 --> 00:03:46,148
a range that is pushed up 
or down if we are healthy,

74
00:03:46,148 --> 00:03:46,987
active,

75
00:03:46,987 --> 00:03:47,95
old,

76
00:03:47,95 --> 00:03:49,663
or overheating.

77
00:03:49,663 --> 00:03:51,522
While water is the healthiest hydrator,

78
00:03:51,522 --> 00:03:52,617
other beverages,

79
00:03:52,617 --> 00:03:55,23
even those with caffeine 
like coffee or tea,

80
00:03:55,23 --> 00:03:57,323
replenish fluids as well.

81
00:03:57,323 --> 00:04:02,306
And water within food makes up 
about a fifth of our daily H20 intake.

82
00:04:02,306 --> 00:04:04,333
Fruits and vegetables like strawberries,

83
00:04:04,333 --> 00:04:05,302
cucumbers,

84
00:04:05,302 --> 00:04:08,874
and even broccoli are over 90% water,

85
00:04:08,874 --> 00:04:14,119
and can supplement liquid intake while
providing valuable nutrients and fiber.

86
00:04:14,119 --> 00:04:17,276
Drinking well might also have various
long-term benefits.

87
00:04:17,276 --> 00:04:21,618
Studies have shown that optimal hydration
can lower the chance of stroke,

88
00:04:21,618 --> 00:04:23,107
help manage diabetes,

89
00:04:23,107 --> 00:04:27,871
and potentially reduce the risk
of certain types of cancer.

90
00:04:27,871 --> 00:04:32,051
No matter what, getting the right amount
of liquid makes a world of difference

91
00:04:32,051 --> 00:04:33,338
in how you'll feel,

92
00:04:33,338 --> 00:04:34,205
think,

93
00:04:34,205 --> 00:04:36,073
and function day to day.

