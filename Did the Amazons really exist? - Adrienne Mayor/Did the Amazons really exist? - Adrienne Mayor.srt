1
00:00:07,779 --> 00:00:09,628
Since the time of Homer,

2
00:00:09,628 --> 00:00:14,578
ancient stories told of fierce warriors 
dwelling beyond the Mediterranean world,

3
00:00:14,578 --> 00:00:18,669
striking fear into the mightiest 
empires of antiquity.

4
00:00:18,669 --> 00:00:22,38
Their exploits were
recounted by many epic poets.

5
00:00:22,38 --> 00:00:27,729
They fought in the legendary Trojan War 
and their grand army invaded Athens.

6
00:00:27,729 --> 00:00:30,3
Jason and the Argonauts 
passed by their shores,

7
00:00:30,3 --> 00:00:32,998
barely avoiding their deadly arrows.

8
00:00:32,998 --> 00:00:37,28
These formidable fighters faced off 
against the greatest champions of myth:

9
00:00:37,28 --> 00:00:38,311
Heracles,

10
00:00:38,311 --> 00:00:39,291
Theseus,

11
00:00:39,291 --> 00:00:41,321
and Achilles.

12
00:00:41,321 --> 00:00:45,841
And every single one 
of these warriors was a woman.

13
00:00:45,841 --> 00:00:51,019
The war-loving Amazons,
&quot;the equals of men&quot; in courage and skill,

14
00:00:51,019 --> 00:00:54,111
were familiar to everyone in
ancient Greece.

15
00:00:54,111 --> 00:00:58,101
Amazon battle scenes decorated 
the Parthenon on the Athenian Acropolis;

16
00:00:58,101 --> 00:01:03,362
paintings and statues of Amazons
adorned temples and public spaces.

17
00:01:03,362 --> 00:01:05,671
Little girls played with Amazon dolls,

18
00:01:05,671 --> 00:01:10,402
and Amazons were a favorite subject 
on Greek vase paintings.

19
00:01:10,402 --> 00:01:12,141
In Greek art and literature,

20
00:01:12,141 --> 00:01:15,131
they were depicted as daring 
and desirable,

21
00:01:15,131 --> 00:01:16,915
but also terrifying and deadly,

22
00:01:16,915 --> 00:01:21,082
and doomed to die 
at the hands of Greek heroes.

23
00:01:21,082 --> 00:01:24,981
Were Amazons merely figures of myth,
or something more?

24
00:01:24,981 --> 00:01:27,517
It was long assumed 
that they were imaginary,

25
00:01:27,517 --> 00:01:29,602
like the cyclops and centaurs.

26
00:01:29,602 --> 00:01:33,113
But curiously enough, 
stories from ancient Egypt,

27
00:01:33,113 --> 00:01:34,324
Persia,

28
00:01:34,324 --> 00:01:35,397
the Middle East,

29
00:01:35,397 --> 00:01:36,463
Central Asia,

30
00:01:36,463 --> 00:01:37,422
India,

31
00:01:37,422 --> 00:01:38,242
and China

32
00:01:38,242 --> 00:01:41,403
also featured Amazon-like warrior women.

33
00:01:41,403 --> 00:01:46,883
And Amazons were described in ancient
historical accounts, not just myths.

34
00:01:46,883 --> 00:01:52,024
Writers like Herodotus, Plato, 
and Strabo never doubted their existence.

35
00:01:52,024 --> 00:01:56,379
So who were the real women 
warriors known as Amazons?

36
00:01:56,379 --> 00:02:00,507
Ancient historians located 
the Amazon homeland in Scythia,

37
00:02:00,507 --> 00:02:02,398
the vast territory stretching from

38
00:02:02,398 --> 00:02:05,901
the Black Sea across 
the steppes of Central Asia.

39
00:02:05,901 --> 00:02:08,695
This immense region was populated 
by nomadic tribes

40
00:02:08,695 --> 00:02:10,919
whose lives centered on horses,

41
00:02:10,919 --> 00:02:11,928
archery,

42
00:02:11,928 --> 00:02:13,12
and warfare.

43
00:02:13,12 --> 00:02:18,535
Their culture flourished for about
1,000 years beginning around 800 BC.

44
00:02:18,535 --> 00:02:22,916
Feared by Greeks, Persians, and
the Chinese, the Scythians left

45
00:02:22,916 --> 00:02:26,636
no written records.
But we can find clues in how

46
00:02:26,636 --> 00:02:30,486
their neighbors described them,
as well as in archaeology.

47
00:02:30,486 --> 00:02:33,356
Scythians' ancestors were 
the first to ride horses

48
00:02:33,356 --> 00:02:36,03
and they invented the recurve bow.

49
00:02:36,03 --> 00:02:41,647
And, because a female mounted archer 
could be as fast and as deadly as a male,

50
00:02:41,647 --> 00:02:44,695
all children were trained 
to ride and shoot.

51
00:02:44,695 --> 00:02:49,208
Women hunted and fought alongside men,
using the same weapons.

52
00:02:49,208 --> 00:02:52,227
The harsh landscape 
and their nomadic lifestyle

53
00:02:52,227 --> 00:02:55,007
created its own form of equality.

54
00:02:55,007 --> 00:03:00,797
This amazed the ancient Greeks, 
whose women led restricted indoor lives.

55
00:03:00,797 --> 00:03:03,888
The earliest stories of the Scythians, 
and Amazons,

56
00:03:03,888 --> 00:03:06,619
may have been exaggerated rumors.

57
00:03:06,619 --> 00:03:10,739
But as the Greeks began to trade around 
the Black Sea and further east,

58
00:03:10,739 --> 00:03:13,646
their portrayals became more realistic.

59
00:03:13,646 --> 00:03:17,539
Early depictions of Amazons showed them 
with Greek weapons and armor.

60
00:03:17,539 --> 00:03:19,758
But in later representations,

61
00:03:19,758 --> 00:03:22,22
they wielded bows and battle-axes,

62
00:03:22,22 --> 00:03:23,347
rode horses,

63
00:03:23,347 --> 00:03:26,101
and wore pointed caps 
and patterned trousers

64
00:03:26,101 --> 00:03:29,238
characteristic of steppe nomads.

65
00:03:29,238 --> 00:03:32,862
Until recently, no one was sure 
how strong the links were

66
00:03:32,862 --> 00:03:36,17
between Scythians 
and the Amazons of Greek myth.

67
00:03:36,17 --> 00:03:40,851
But recent archaeological discoveries 
have provided ample evidence.

68
00:03:40,851 --> 00:03:46,234
More than 1,000 ancient Scythian kurgans,
or burial mounds, have been excavated,

69
00:03:46,234 --> 00:03:48,817
containing skeletons and weapons.

70
00:03:48,817 --> 00:03:51,065
Archaeologists had previously assumed

71
00:03:51,065 --> 00:03:54,164
that weapons could only 
belong to male warriors.

72
00:03:54,164 --> 00:03:56,781
But modern DNA analysis so far

73
00:03:56,781 --> 00:03:59,825
has revealed that about 300 skeletons 
buried with weapons

74
00:03:59,825 --> 00:04:04,092
belong to females ranging
in age from 10 to 45,

75
00:04:04,092 --> 00:04:07,166
and more are being found every year.

76
00:04:07,166 --> 00:04:09,543
The women's skeletons 
show battle injuries:

77
00:04:09,543 --> 00:04:11,762
ribs slashed by swords,

78
00:04:11,762 --> 00:04:14,211
skulls bashed by battle-axes,

79
00:04:14,211 --> 00:04:17,473
and arrows embedded in bones.

80
00:04:17,473 --> 00:04:19,744
In classical art and writings,

81
00:04:19,744 --> 00:04:24,654
the fearsome Amazons were always portrayed
as brave and heroic.

82
00:04:24,654 --> 00:04:27,295
In male-dominated classical Greece, 
however,

83
00:04:27,295 --> 00:04:34,162
the very idea of strong women who gloried 
in freedom and war aroused mixed feelings.

84
00:04:34,162 --> 00:04:38,044
And yet, the Greeks were also drawn
to egalitarian ideals.

85
00:04:38,044 --> 00:04:42,167
Is it possible that the mythic realm 
of thrilling Amazon tales

86
00:04:42,167 --> 00:04:46,194
was a way to imagine women
and men as equal companions?

