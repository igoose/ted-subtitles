1
00:00:06,898 --> 00:00:09,219
Cigarettes aren’t good for us.
2
00:00:09,219 --> 00:00:13,865
That’s hardly news--we’ve known 
about the dangers of smoking for decades.
3
00:00:13,865 --> 00:00:16,86
But how exactly do cigarettes harm us?
4
00:00:16,86 --> 00:00:19,192
Let’s look at what happens 
as their ingredients
5
00:00:19,192 --> 00:00:20,851
make their way through our bodies,
6
00:00:20,851 --> 00:00:25,478
and how we benefit physically 
when we finally give up smoking.
7
00:00:25,478 --> 00:00:27,358
With each inhalation,
8
00:00:27,358 --> 00:00:31,158
smoke brings its more than 5,000 
chemical substances
9
00:00:31,158 --> 00:00:33,766
into contact with the body’s tissues.
10
00:00:33,766 --> 00:00:37,444
From the start, tar, 
a black, resinous material,
11
00:00:37,444 --> 00:00:39,825
begins to coat the teeth and gums,
12
00:00:39,825 --> 00:00:43,708
damaging tooth enamel, 
and eventually causing decay.
13
00:00:43,708 --> 00:00:47,721
Over time, smoke also damages 
nerve-endings in the nose,
14
00:00:47,721 --> 00:00:50,006
causing loss of smell.
15
00:00:50,006 --> 00:00:51,815
Inside the airways and lungs,
16
00:00:51,815 --> 00:00:54,604
smoke increases 
the likelihood of infections,
17
00:00:54,604 --> 00:00:59,277
as well as chronic diseases
like bronchitis and emphysema.
18
00:00:59,277 --> 00:01:01,707
It does this by damaging the cilia,
19
00:01:01,707 --> 00:01:07,067
tiny hairlike structures whose job it is 
to keep the airways clean.
20
00:01:07,067 --> 00:01:09,504
It then fills the alveoli,
21
00:01:09,504 --> 00:01:13,654
tiny air sacs that enable the exchange 
of oxygen and carbon dioxide
22
00:01:13,654 --> 00:01:15,258
between the lungs and blood.
23
00:01:15,258 --> 00:01:20,251
A toxic gas called carbon monoxide 
crosses that membrane into the blood,
24
00:01:20,251 --> 00:01:21,958
binding to hemoglobin
25
00:01:21,958 --> 00:01:23,561
and displacing the oxygen
26
00:01:23,561 --> 00:01:26,771
it would usually have transported
around the body.
27
00:01:26,771 --> 00:01:30,38
That’s one of the reasons smoking 
can lead to oxygen deprivation
28
00:01:30,38 --> 00:01:32,954
and shortness of breath.
29
00:01:32,954 --> 00:01:35,121
Within about 10 seconds,
30
00:01:35,121 --> 00:01:38,897
the bloodstream carries a stimulant 
called nicotine to the brain,
31
00:01:38,897 --> 00:01:42,413
triggering the release of dopamine 
and other neurotransmitters
32
00:01:42,413 --> 00:01:43,689
including endorphins
33
00:01:43,689 --> 00:01:47,684
that create the pleasurable sensations 
which make smoking highly addictive.
34
00:01:47,684 --> 00:01:50,043
Nicotine and other chemicals 
from the cigarette
35
00:01:50,043 --> 00:01:53,006
simultaneously cause constriction 
of blood vessels
36
00:01:53,006 --> 00:01:55,942
and damage their delicate 
endothelial lining,
37
00:01:55,942 --> 00:01:57,675
restricting blood flow.
38
00:01:57,675 --> 00:02:00,957
These vascular effects lead 
to thickening of blood vessel walls
39
00:02:00,957 --> 00:02:03,471
and enhance blood platelet stickiness,
40
00:02:03,471 --> 00:02:06,055
increasing the likelihood 
that clots will form
41
00:02:06,055 --> 00:02:08,929
and trigger heart attacks and strokes.
42
00:02:08,929 --> 00:02:12,689
Many of the chemicals inside cigarettes 
can trigger dangerous mutations
43
00:02:12,689 --> 00:02:15,965
in the body’s DNA that make cancers form.
44
00:02:15,965 --> 00:02:18,955
Additionally, ingredients like arsenic 
and nickel
45
00:02:18,955 --> 00:02:21,524
may disrupt the process of DNA repair,
46
00:02:21,524 --> 00:02:25,443
thus compromising the body’s ability 
to fight many cancers.
47
00:02:25,443 --> 00:02:29,175
In fact, about one of every three 
cancer deaths in the United States
48
00:02:29,175 --> 00:02:31,218
is caused by smoking.
49
00:02:31,218 --> 00:02:33,571
And it’s not just lung cancer.
50
00:02:33,571 --> 00:02:36,938
Smoking can cause cancer 
in multiple tissues and organs,
51
00:02:36,938 --> 00:02:38,745
as well as damaged eyesight
52
00:02:38,745 --> 00:02:40,901
and weakened bones.
53
00:02:40,901 --> 00:02:43,11
It makes it harder 
for women to get pregnant.
54
00:02:43,11 --> 00:02:46,088
And in men,
it can cause erectile dysfunction.
55
00:02:46,088 --> 00:02:47,785
But for those who quit smoking,
56
00:02:47,785 --> 00:02:49,763
there’s a huge positive upside
57
00:02:49,763 --> 00:02:53,328
with almost immediate 
and long-lasting physical benefits.
58
00:02:53,328 --> 00:02:57,01
Just 20 minutes after 
a smoker’s final cigarette,
59
00:02:57,01 --> 00:03:00,841
their heart rate and blood pressure
begin to return to normal.
60
00:03:00,841 --> 00:03:04,259
After 12 hours, 
carbon monoxide levels stabilize,
61
00:03:04,259 --> 00:03:07,023
increasing the blood’s 
oxygen-carrying capacity.
62
00:03:07,023 --> 00:03:08,54
A day after ceasing,
63
00:03:08,54 --> 00:03:13,894
heart attack risk begins to decrease as
blood pressure and heart rates normalize.
64
00:03:13,894 --> 00:03:15,17
After two days,
65
00:03:15,17 --> 00:03:20,094
the nerve endings responsible 
for smell and taste start to recover.
66
00:03:20,094 --> 00:03:23,52
Lungs become healthier 
after about one month,
67
00:03:23,52 --> 00:03:25,875
with less coughing 
and shortness of breath.
68
00:03:25,875 --> 00:03:28,999
The delicate hair-like cilia 
in the airways and lungs
69
00:03:28,999 --> 00:03:31,077
start recovering within weeks,
70
00:03:31,077 --> 00:03:35,34
and are restored after 9 months, 
improving resistance to infection.
71
00:03:35,34 --> 00:03:37,717
By the one-year anniversary of quitting,
72
00:03:37,717 --> 00:03:42,761
heart disease risk plummets to half
as blood vessel function improves.
73
00:03:42,761 --> 00:03:44,025
Five years in,
74
00:03:44,025 --> 00:03:46,845
the chance of a clot forming
dramatically declines,
75
00:03:46,845 --> 00:03:49,751
and the risk of stroke 
continues to reduce.
76
00:03:49,751 --> 00:03:53,352
After ten years, the chances 
of developing fatal lung cancer
77
00:03:53,352 --> 00:03:55,398
go down by 50%,
78
00:03:55,398 --> 00:04:00,169
probably because the body’s ability 
to repair DNA is once again restored.
79
00:04:00,169 --> 00:04:04,008
Fifteen years in, the likelihood 
of developing coronary heart disease
80
00:04:04,008 --> 00:04:07,522
is essentially the same 
as that of a non-smoker.
81
00:04:07,522 --> 00:04:10,818
There’s no point pretending 
this is all easy to achieve.
82
00:04:10,818 --> 00:04:13,64
Quitting can lead to anxiety 
and depression,
83
00:04:13,64 --> 00:04:15,582
resulting from nicotine withdrawal.
84
00:04:15,582 --> 00:04:19,292
But fortunately, 
such effects are usually temporary.
85
00:04:19,292 --> 00:04:23,98
And quitting is getting easier,
thanks to a growing arsenal of tools.
86
00:04:23,98 --> 00:04:26,426
Nicotine replacement therapy through gum,
87
00:04:26,426 --> 00:04:27,655
skin patches,
88
00:04:27,655 --> 00:04:28,439
lozenges,
89
00:04:28,439 --> 00:04:29,352
and sprays
90
00:04:29,352 --> 00:04:32,345
may help wean smokers off cigarettes.
91
00:04:32,345 --> 00:04:35,458
They work by stimulating 
nicotine receptors in the brain
92
00:04:35,458 --> 00:04:37,85
and thus preventing withdrawal symptoms,
93
00:04:37,85 --> 00:04:40,758
without the addition 
of other harmful chemicals.
94
00:04:40,758 --> 00:04:42,47
Counselling and support groups,
95
00:04:42,47 --> 00:04:44,413
cognitive behavioral therapy,
96
00:04:44,413 --> 00:04:46,606
and moderate intensity exercise
97
00:04:46,606 --> 00:04:50,061
also help smokers stay cigarette-free.
98
00:04:50,061 --> 00:04:51,608
That’s good news,
99
00:04:51,608 --> 00:04:56,256
since quitting puts you and your body 
on the path back to health.

