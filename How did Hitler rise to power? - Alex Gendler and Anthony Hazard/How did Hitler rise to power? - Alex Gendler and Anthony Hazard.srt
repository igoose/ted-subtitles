1
00:00:06,776 --> 00:00:08,337
How did Adolf Hitler,

2
00:00:08,337 --> 00:00:12,747
a tyrant who orchestrated one of the
largest genocides in human history,

3
00:00:12,747 --> 00:00:16,506
rise to power in a democratic country?

4
00:00:16,506 --> 00:00:19,738
The story begins at the end
of World War I.

5
00:00:19,738 --> 00:00:22,798
With the successful 
Allied advance in 1918,

6
00:00:22,798 --> 00:00:26,256
Germany realized the war was unwinnable

7
00:00:26,256 --> 00:00:29,566
and signed an armistice 
ending the fighting.

8
00:00:29,566 --> 00:00:31,698
As its imperial government collapsed,

9
00:00:31,698 --> 00:00:36,006
civil unrest and worker strikes
spread across the nation.

10
00:00:36,006 --> 00:00:37,896
Fearing a Communist revolution,

11
00:00:37,896 --> 00:00:41,557
major parties joined 
to suppress the uprisings,

12
00:00:41,557 --> 00:00:45,348
establishing the parliamentary
Weimar Republic.

13
00:00:45,348 --> 00:00:47,477
One of the new government's first tasks

14
00:00:47,477 --> 00:00:51,617
was implementing the peace treaty
imposed by the Allies.

15
00:00:51,617 --> 00:00:56,298
In addition to losing over a tenth
of its territory and dismantling its army,

16
00:00:56,298 --> 00:01:02,258
Germany had to accept full responsibility
for the war and pay reparations,

17
00:01:02,258 --> 00:01:06,017
debilitating its already weakened economy.

18
00:01:06,017 --> 00:01:10,928
All this was seen as a humiliation
by many nationalists and veterans.

19
00:01:10,928 --> 00:01:13,719
They wrongly believed the war 
could have been won

20
00:01:13,719 --> 00:01:19,407
if the army hadn't been betrayed
by politicians and protesters.

21
00:01:19,407 --> 00:01:22,47
For Hitler, these views became obsession,

22
00:01:22,47 --> 00:01:28,37
and his bigotry and paranoid delusions
led him to pin the blame on Jews.

23
00:01:28,37 --> 00:01:32,5
His words found resonance in a society
with many anti-Semitic people.

24
00:01:32,5 --> 00:01:35,31
By this time, hundreds 
of thousands of Jews

25
00:01:35,31 --> 00:01:37,947
had integrated into German society,

26
00:01:37,947 --> 00:01:42,62
but many Germans continued to perceive
them as outsiders.

27
00:01:42,62 --> 00:01:47,582
After World War I, Jewish success led
to ungrounded accusations

28
00:01:47,582 --> 00:01:51,029
of subversion and war profiteering.

29
00:01:51,029 --> 00:01:54,399
It can not be stressed enough that these
conspiracy theories

30
00:01:54,399 --> 00:01:56,191
were born out of fear,

31
00:01:56,191 --> 00:01:57,099
anger,

32
00:01:57,099 --> 00:01:58,351
and bigotry,

33
00:01:58,351 --> 00:01:59,96
not fact.

34
00:01:59,96 --> 00:02:02,527
Nonetheless, Hitler found 
success with them.

35
00:02:02,527 --> 00:02:05,97
When he joined a small nationalist
political party,

36
00:02:05,97 --> 00:02:09,839
his manipulative public speaking
launched him into its leadership

37
00:02:09,839 --> 00:02:12,88
and drew increasingly larger crowds.

38
00:02:12,88 --> 00:02:16,149
Combining anti-Semitism with 
populist resentment,

39
00:02:16,149 --> 00:02:19,749
the Nazis denounced both Communism
and Capitalism

40
00:02:19,749 --> 00:02:24,82
as international Jewish conspiracies
to destroy Germany.

41
00:02:24,82 --> 00:02:27,851
The Nazi party was not initially popular.

42
00:02:27,851 --> 00:02:31,34
After they made an unsuccessful attempt
at overthrowing the government,

43
00:02:31,34 --> 00:02:33,16
the party was banned,

44
00:02:33,16 --> 00:02:35,68
and Hitler jailed for treason.

45
00:02:35,68 --> 00:02:37,931
But upon his release about a year later,

46
00:02:37,931 --> 00:02:41,201
he immediately began to rebuild
the movement.

47
00:02:41,201 --> 00:02:45,38
And then, in 1929, 
the Great Depression happened.

48
00:02:45,38 --> 00:02:49,061
It led to American banks withdrawing
their loans from Germany,

49
00:02:49,061 --> 00:02:54,01
and the already struggling German economy
collapsed overnight.

50
00:02:54,01 --> 00:02:56,442
Hitler took advantage 
of the people's anger,

51
00:02:56,442 --> 00:02:58,343
offering them convenient scapegoats

52
00:02:58,343 --> 00:03:02,221
and a promise to restore Germany's
former greatness.

53
00:03:02,221 --> 00:03:06,042
Mainstream parties proved
unable to handle the crisis

54
00:03:06,042 --> 00:03:11,131
while left-wing opposition was too
fragmented by internal squabbles.

55
00:03:11,131 --> 00:03:15,431
And so some of the frustrated public
flocked to the Nazis,

56
00:03:15,431 --> 00:03:22,652
increasing their parliamentary votes from
under 3% to over 18% in just two years.

57
00:03:22,652 --> 00:03:25,485
In 1932, Hitler ran for president,

58
00:03:25,485 --> 00:03:30,273
losing the election to decorated war hero
General von Hindenburg.

59
00:03:30,273 --> 00:03:35,855
But with 36% of the vote, Hitler had
demonstrated the extent of his support.

60
00:03:35,855 --> 00:03:38,856
The following year, advisors 
and business leaders

61
00:03:38,856 --> 00:03:42,882
convinced Hindenburg to appoint Hitler
as Chancellor,

62
00:03:42,882 --> 00:03:46,783
hoping to channel his popularity
for their own goals.

63
00:03:46,783 --> 00:03:50,132
Though the Chancellor was only
the administrative head of parliament,

64
00:03:50,132 --> 00:03:54,082
Hitler steadily expanded the power
of his position.

65
00:03:54,082 --> 00:03:56,932
While his supporters formed 
paramilitary groups

66
00:03:56,932 --> 00:03:59,323
and fought protestors in streets.

67
00:03:59,323 --> 00:04:02,943
Hitler raised fears 
of a Communist uprising

68
00:04:02,943 --> 00:04:06,873
and argued that only he could restore
law and order.

69
00:04:06,873 --> 00:04:08,824
Then in 1933,

70
00:04:08,824 --> 00:04:13,843
a young worker was convicted of
setting fire to the parliament building.

71
00:04:13,843 --> 00:04:16,663
Hitler used the event to convince
the government

72
00:04:16,663 --> 00:04:19,383
to grant him emergency powers.

73
00:04:19,383 --> 00:04:23,384
Within a matter of months,
freedom of the press was abolished,

74
00:04:23,384 --> 00:04:25,245
other parties were disbanded,

75
00:04:25,245 --> 00:04:28,775
and anti-Jewish laws were passed.

76
00:04:28,775 --> 00:04:33,424
Many of Hitler's early radical supporters
were arrested and executed,

77
00:04:33,424 --> 00:04:35,483
along with potential rivals,

78
00:04:35,483 --> 00:04:38,975
and when President Hindenburg died
in August 1934,

79
00:04:38,975 --> 00:04:42,304
it was clear there would be 
no new election.

80
00:04:42,304 --> 00:04:47,564
Disturbingly, many of Hitler's early 
measures didn't require mass repression.

81
00:04:47,564 --> 00:04:50,613
His speeches exploited 
people's fear and ire

82
00:04:50,613 --> 00:04:54,574
to drive their support behind him
and the Nazi party.

83
00:04:54,574 --> 00:04:57,104
Meanwhile, businessmen and intellectuals,

84
00:04:57,104 --> 00:04:59,745
wanting to be on the right side 
of public opinion,

85
00:04:59,745 --> 00:05:01,394
endorsed Hitler.

86
00:05:01,394 --> 00:05:03,244
They assured themselves and each other

87
00:05:03,244 --> 00:05:06,294
that his more extreme rhetoric 
was only for show.

88
00:05:06,294 --> 00:05:09,976
Decades later, Hitler's rise remains 
a warning

89
00:05:09,976 --> 00:05:15,096
of how fragile democratic institutions
can be in the face of angry crowds

90
00:05:15,096 --> 00:05:19,167
and a leader willing to feed their anger
and exploit their fears.

